package com.hayevska;

import org.apache.logging.log4j.*;
public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        logger1.trace("Trace sms");
        logger1.debug("Debug sms");
        logger1.info("Info sms");
        logger1.warn("Warn sms");
        logger1.error("Error sms");
        logger1.fatal("Fatal sms");
    }
}
